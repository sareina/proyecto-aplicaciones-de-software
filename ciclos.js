let i = 1;
//Ciclo
while (i <= 10){
    console.log(i);
    //Instrucción rompimiento
    i++;
}

let numero = 1;
while (numero <= 30){
    if (numero % 2 == 0){
        console.log(`${numero} es par`);
    }
    numero++;
}

// DO/WHILE
let a = 4;
do {
    console.log(a);
    a++;
} while(a<=5);

//For es más compacto 

let x = 0;
for ( ; x <= 100;) {
    console.log(x);
    x += 10
}

for (let x = 0, y = 5;x <= 100 && y != 2; x += 10, y--) {
    console.log(x,y);
}