let cadena1 = "Universidad";
console.log(cadena1);

let cadena2 = "I don't know";
console.log(cadena2);

let cadena3 = "Universidad\nSanto\nTomas";
console.log(cadena3);

let cadena4 = "El" + "Paseo" + "6";
console.log(cadena4);
cadena4 += ": La venganza de los rolos - ";
console.log(cadena4);


cadena4 = cadena4 + 2017;
console.log(cadena4);

let cadena5 ="456";
let numero1 = parseInt(cadena5);
console.log(cadena5, typeof cadena5);
console.log(numero1, typeof numero1);

let longitud1 = cadena4.length;
console.log(longitud1);

let caracter = cadena4[0];
console.log(caracter);

let pelicula= "star wars";
let anio = 1978;
let director = "George Lucas" 

let titulo1 = pelicula + "-" + anio + ", director: " + director;
console.log(titulo1);

let titulo2 = `${pelicula} - ${anio}, director: ${director} - ${4+5}`;
console.log(titulo2);
