let numeros1 = [];
console.log(numeros1);

let numeros2 = [10, 20 ,30];
console.log(numeros2);

let datos = [10, 3.2, 'cata', true];
console.log(datos);

console.log(datos[6]);
console.log(datos[1]);
console.log();

if (typeof datos[6] == "undefined") {
    console.log("No existe");
    
}

datos [2] = "Katalina";
console.log(datos);

let datos1 = [1,2,3];
let datos2 = datos1;
datos1[0] = 10;
console.log(datos1);
console.log(datos2);

let num1 = [1,2,3];
let num2 = [... num1];

num1[0] = 10;

console.log(num1);
console.log(num2);

console.log("FUNCIONES");
let numbers = [10, 20, 30];
console.log(numbers);

numbers.unshift(40);
console.log(numbers);

numbers.push(20);
console.log(numbers);

numbers.shift();
console.log(numbers);

numbers.pop();
console.log(numbers);
//Insertar en una posicion
numbers.splice(1,0,80);
console.log(numbers);
//Eliminar en una posicion
numbers.splice(1,1);
console.log(numbers);

let emojis = ["😍","😋"];
console.log(emojis);
if(emojis[0] == "😍"){
    console.log("corazoncito");
}

let nombres1 = ['Universidad'];
let nombres2 = ['Santo','Tomás'];
let nombres3 = nombres1.concat(nombres2);
console.log(nombres3);

//FUNCIONES
let elements = [100, 200, 300];
let nuevo1 = elements.map((e) => e + 1);
console.log(elements);
console.log(nuevo1);

let nuevo1 = elements.map((e) => {
    console.log("Transformando");
    let x = 0;
    x = e * 3;
    return (x);
});

let r1 = elements.reduce((x,y)=> x + y);
console.log(r1);


let x = 4;
console.log(typeof x);

x = "Cata";
console.log(typeof x);

x = true;
console.log(typeof x);